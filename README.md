# Sehat (Save Our Heart)

Aplikasi mobile web progresif ([progressive web app](https://developers.google.com/web/progressive-web-apps/)) untuk pertolongan pertama pada henti jantung. Untuk mengirimkan notifikasi secara real-time, aplikasi ini memanfaatkan layanan gratis [Firebase Cloud Messaging](https://firebase.google.com/docs/cloud-messaging).

Fitur bagi pasien:

* Mengirimkan notifikasi darurat pada pengguna lain yang terdaftar
* Mengaktifkan audio minta tolong untuk menarik perhatian orang sekitar
* Menyediakan panduan Bantuan Hidup Dasar (BHD) Resusistasi Jantung Paru (RJP)

Fitur bagi penerima notifikasi:

* Melihat peta lokasi pasien (berdasarkan GPS)
* Melihat daftar nama & alamat rumah sakit terdekat dengan pasien

Rencana pengembangan fitur:

* Menampilkan nomor telepon rumah sakit
* Menunda pengiriman notifikasi jika tidak ada koneksi internet (offline) dan otomatis mengirimkan saat ada koneksi (online)

## Panduan Instalasi

Berikut langkah-langkah untuk menggunakan aplikasi ini pada smartphone/PC:

* Pastikan smartphone/PC Anda memiliki browser [Google Chrome](https://www.google.com/chrome).
* Buka URL https://sehat.herokuapp.com pada Google Chrome
* Lakukan registrasi atau login jika telah registrasi sebelumnya
* Pada smartphone, Anda dapat menambahkan shortcut dengan mengklik tombol option (icon tiga titik vertikal) dan memilih menu **Add to homescreen**


# Panduan Pengembangan

Untuk mengembangkan aplikasi ini dibutuhkan kakas & pustaka sebagai berikut:

* Node.js >= 6.11.0 https://nodejs.org/en/
* MongoDB >= 3.2.11 https://www.mongodb.com
* Git (opsional) https://git-scm.com
* Heroku Toolbelt https://cli.heroku.com (opsional. Untuk men-deploy aplikasi di https://www.heroku.com/)

Cara menjalankan aplikasi pada mesin lokal:

* Clone repositori ini dengan `git clone` atau unduh dalam format `.zip` dan ekstrak
* Buat file konfigurasi `.env` dengan mengubah nama `.env.example` dan menyesuaikan isinya:
    
    * `APP_URL`: URL aplikasi yang akan dikirimkan sebagai link pada notifkasi/alert
    * `MONGO_URL`: URL database server MongoDB
    * `GOOGLE_PLACES_API_KEY`: API key untuk Google Places
    * `FIREBASE_ADMIN_ACCOUNT`: konten dari Firebase Admin service account file. Informasi lebih lengkap dapat dibaca di https://firebase.google.com/docs/admin/setup

* Dari dalam direktori proyek, jalankan `npm install`
* Setelah instalasi selesai, jalankan `npm start`
* Akses aplikasi di `http://127.0.0.1:5000` (Firebase Cloud Messaging tidak bisa berfungsi pada `http://localhost`)


Cara menjalankan aplikasi pada server, sama dengan tambahan:

* Sesuaikan `.env` dengan kondisi server
* Jika melakukan deployment pada Heroku, pindahkan variables dari `.env` sebagai environment variables https://devcenter.heroku.com/articles/config-vars
* Petunjuk mengenai cara deploy pada Heroku dapat dilihat di https://devcenter.heroku.com/articles/getting-started-with-nodejs#deploy-the-app

## Kredit

Aplikasi ini dbuat berdasarkan rancangan Mahasiswa Pascasarjana Fakultas Ilmu Keperawatan Universitas Indonesia:

1. Andi Sudrajat, NPM. 1706096216, Peminatan Keperawatan KMB
2. Casman, NPM. 1706096241, Peminatan Keperawatan Anak
3. Ita Pursitasari, NPM  1706096374, Peminatan Keperawatan Anak
4. Juliana Gracia Eka P. Massie , NPM. 1706006933, Peminatan Keperawatan KMB
5. Ni Luh Putu Dian Yunita Sari	, NPM. 1706096443, Peminatan Keperawatan Komunitas
6. Siti Aisah, NPM. 1706096563, Peminatan Keperawatan KMB
7. Suyatno, NPM. 1706096582, Peminatan Keperawatan Jiwa

Sumber eksternal:

* Audio permintaan tolong: https://www.youtube.com/watch?v=iPRD2cUoAQY
* Icon aplikasi: https://www.flaticon.com


## Lisensi

The MIT License (MIT)
=====================

Copyright © 2017 Yohanes Gultom

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
