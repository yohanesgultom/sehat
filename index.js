const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const admin = require('firebase-admin');
const request = require('request');
const PORT = process.env.PORT || 5000;

// env variable
require('dotenv').config();
console.log(`APP_URL=${process.env.APP_URL}`);
console.log(`MONGODB_URI=${process.env.MONGODB_URI}`);

// connect to database
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI, {useMongoClient: true});

// create schemas
let UserSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true
  },
  messagingToken: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  members: [String],
}, { timestamps: true });
let User = mongoose.model('User', UserSchema);

let AlertSchema = new mongoose.Schema({
  senderId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    index: true,
  },
  message: {
    type: String,
    required: true,
  },
  lat: {
    type: String,
  },
  lng: {
    type: String,
  },
  status: {
    type: Number,
    default: 1,
  }
}, { timestamps: true });
// automatically clear records
AlertSchema.index({ createdAt: 1 }, { expireAfterSeconds: 3600 });
let Alert = mongoose.model('Alert', AlertSchema);


let CacheSchema = new mongoose.Schema({
  key: {
    type: String,
    required: true,
    unique: true,
  },
  value: {
    type: String,
    required: true,
  },
}, { timestamps: true });
// automatically clear records
CacheSchema.index({ createdAt: 1 }, { expireAfterSeconds: 86400 });
let Cache = mongoose.model('Cache', CacheSchema);



// Firebase Admin

const serviceAccount = JSON.parse(process.env.FIREBASE_ADMIN_ACCOUNT);
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://emergency-2ca6a.firebaseio.com"
});

// Express APIs

const app = express();
app.use(require('heroku-ssl-redirect')());
app.use(express.static(path.join(__dirname, 'public')))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

/**
 * Register new user
 */
app.post('/register', (req, res) => {
  bcrypt.hash(req.body.password, 10, function (err, hash) {
    if (err) {
      res.status(500).send(err);
    } else {
      let user = new User({
        email: req.body.email,
        password: hash,
        messagingToken: req.body.messagingToken,
      });
      user
        .save()
        .then(newUser => {
          newUser.password = undefined;
          res.json(newUser);
        })
        .catch(err => {
          res.status(400).send(err.errmsg);
        });  
    }
  });
});


/**
 * Login as existing user
 */
app.post('/login', (req, res) => {
  User
    .findOne({ email: req.body.email })
    .then(user => {
      return new Promise((resolve, reject) => {
        if (!user) {
          reject(`${req.body.email} is not found. Please register first`);
        }
        bcrypt.compare(req.body.password, user.password, function (err, valid) {
          if (err) {
            reject(err);
          } else if (!valid) {
            reject('Invalid email/password');
          } else {
            resolve(user);
          }
        });
      });
    })
    .then(user => {
      // update messagingToken if different
      if (req.body.messagingToken
        && user.messagingToken != req.body.messagingToken) {
        user.messagingToken = req.body.messagingToken;
        return user.save();
      } else {
        return Promise.resolve(user);
      }
    })
    .then(user => {
      user.password = undefined;
      res.json(user);
    })
    .catch(err => {
      console.error(err);
      res.status(500).send(err);
    });
});

/**
 * Add member
 */
app.post('/addMember', (req, res) => {  
  User
    .findById(req.body.userId)
    .then(user => {
      // validate token
      if (user.messagingToken != req.body.messagingToken) {
        res.status(401).send('Unauthorized operation');
      } else {
        User
          .findOne({email: req.body.memberEmail})
          .then(member => {
            if (!member) {
              return Promise.reject({status: 404, message: 'Not found'});
            } else if (user.members.indexOf(member.email) > -1) {
              return Promise.reject({ status: 400, message: 'Already exists: ' + member.email});       
            } else {
              user.members.push(member.email);
              return user.save();
            }
          })
          .then(updatedUser => {
            res.json(updatedUser.members);
          })
          .catch(err => {
            if (err.status && err.message) {
              res.status(err.status).send(err.message);
            } else {
              res.status(500).send(err);
            }
          });
      }
    })
    .catch(err => {
      res.status(500).send(err);
    });
});


/**
 * Remove member
 */
app.post('/removeMember', (req, res) => {
  User
    .findById(req.body.userId)
    .then(user => {
      let memberIndex = user.members.indexOf(req.body.memberEmail);
      // validate token      
      if (user.messagingToken != req.body.messagingToken) {
        res.status(401).send('Unauthorized operation');
      } else if (memberIndex < 0) {
        res.status(404).send('Member not found');
      } else {
        // remove member
        user.members.splice(memberIndex, 1);
        user
          .save()
          .then(updatedUser => {
            res.json(updatedUser.members);
          })
          .catch(err => {
            res.status(500).send(err);
          });
      }
    })
    .catch(err => {
      res.status(500).send(err);
    });
});


/**
 * Send emergency notification to members
 */
app.post('/sendAlert', (req, res) => {
  let registrationTokens = [];
  let patient;
  User
    .findById(req.body.userId)
    .then(user => {
      patient = user;      
      // validate token
      if (user.messagingToken != req.body.messagingToken) {
        res.status(401).send('Unauthorized operation');
      } else {
        // find targets
        User
          .find({
            email: {$in: user.members}
          })
          .then(members => {
            if (!members || members.length <= 0) {
              return Promise.reject({
                status: 404,
                message: 'No member found'
              });
            } else {
              registrationTokens = members.map(m => m.messagingToken);
              let alert = new Alert({
                senderId: user._id,
                message: `${patient.email} sent emergency alert. Please contact or find him/her or call ambulance to given location as soon as possible`,
                lat: '0.0',
                lng: '0.0',
              });
              // include location if available
              if (req.body.lat && req.body.lng) {
                alert.lat = req.body.lat;
                alert.lng = req.body.lng;
              }
              return alert.save();
            }
          })
          .then(alert => {
            // send messages
            let payload = {
              data: {
                id: alert.id,
                sender: patient.email,
                title: `Emergency alert from ${patient.email}`,
                body: alert.message,
                lng: alert.lng,
                lat: alert.lat,                
                createdAt: alert.createdAt.toString(),
                url: `${process.env.APP_URL}/#viewAlert/${alert.id}`,
                icon: `${process.env.APP_URL}/images/icons/icon-256x256.png`
              }
            }
            return admin.messaging().sendToDevice(registrationTokens, payload);
          })
          .then(response => {
            console.log('Successfully sent message:', response);
            res.json(response);
          })          
          .catch(err => {
            // TODO: set alert.status = 0 to support auto resending
            if (err.status && err.message) {
              res.status(err.status).send(err.message);
            } else {
              res.status(500).send(err);
            }
          });
      }
    })
    .catch(err => {
      res.status(500).send(err);
    });
});

/**
 * Update messagingToken
 */
app.post('/updateToken', (req, res) => {
  User
    .findById(req.body.userId)
    .then(user => {
      // validate token
      if (user.messagingToken != req.body.oldMessagingToken) {
        res.status(401).send('Unauthorized operation');
      } else {
        user.messagingToken = req.body.messagingToken;
        user
          .save()
          .then(updatedUser => {
            updatedUser.password = undefined;
            res.json(updatedUser);
          })
          .catch(err => {
            res.status(500).send(err);
          });
      }
    })
    .catch(err => {
      res.status(500).send(err);
    });
});


/**
 * Get nearby hospitals
 */
app.get('/nearbyHospitals', (req, res) => {
  let limit = 10;
  User
    .findById(req.query.userId)
    .then(user => {
      // validate token
      if (user.messagingToken != req.query.messagingToken) {
        res.status(401).send('Unauthorized operation');
      } else {
        let lat, lng, cacheKey;
        try {
          lat = parseFloat(req.query.lat);
          lng = parseFloat(req.query.lng);
          cacheKey = `${lat},${lng}`;
        } catch (e) {
          res.status(400).send(e);
        }

        // check cache
        Cache
          .findOne({key: cacheKey})
          .then(function(cache) {
            if (cache) {
              try {        
                console.log('get from cache');        
                let list = JSON.parse(cache.value);
                res.json(list.slice(0, Math.min(list.length, limit)));
              } catch (e) {
                res.status(500).send(e);
              }              
            } else {
              // query Google Places API
              // TODO: get phone number?
              let url = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${lat},${lng}&rankby=distance&type=hospital&keyword=(RS) OR (rumah sakit)&key=${process.env.GOOGLE_PLACES_API_KEY}`;
              request.get({ url: url, json: true }, function (error, response, body) {
                if (error) {
                  console.error(error);
                  res.status(500).send(error);
                } else if (!body || body.status != 'OK') {
                  res.status(500).send(new Error(body.status));
                } else {
                  console.log(body.results);
                  if (body.results.length <= 0) {
                    res.status(404).send(new Error('No nearby hospitals'));
                  } else {                    
                    let mapped = body.results.map(function (r) {
                      return {
                        id: r.id,
                        placeId: r.place_id,
                        name: r.name,
                        address: r.vicinity,
                      }
                    });
                    // store cache
                    let newCache = new Cache({ key: cacheKey, value: JSON.stringify(mapped) });
                    newCache
                      .save()
                      .then(function(obj) {
                        // return limited results
                        res.json(mapped.slice(0, Math.min(mapped.length, limit)));
                      })
                      .catch(console.error);
                  }
                }
              });
              
            }
          })
          .catch(function(err) {
            console.error(err);
            res.status(500).send(err);
          });
      }
    })
    .catch(err => {
      console.error(err);
      res.status(500).send(err);
    });
});


// start listening
app.listen(PORT, () => console.log(`Listening on http://127.0.0.1:${ PORT }`))
