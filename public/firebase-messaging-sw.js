// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-messaging.js');
importScripts('/scripts/localforage.min.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
    'messagingSenderId': '916281526247'
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

// localforage
// TODO: try to make it DRY
localforage.config({
    name: 'sehat',
    storeName: 'sehat_store',
});

// accept message when on background
messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    const notificationTitle = payload.data.title;
    const notificationOptions = {
        body: payload.data.body,
        icon: payload.data.icon,
    };

    // click handler
    self.addEventListener('notificationclick', function (event) {
        event.notification.close();
        event.waitUntil(self.clients.openWindow(payload.data.url));
    });
    
    // save Alert in storage
    let alerts = [];    
    localforage
        .getItem('alerts')
        .then(function(result) {
            alerts = result ? result : [];
            alerts.push(payload.data);
            return localforage.setItem('alerts', alerts);
        })
        .then(function() {            
            console.log('[firebase-messaging-sw.js] alerts updated', alerts);
        })
        .catch(console.error);    
    
    return self.registration.showNotification(notificationTitle, notificationOptions);
});