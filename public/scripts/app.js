// for resolving google map script
var resolveFunc = null;
var initGoogleMap = function () { resolveFunc(); };

(function() {
  'use strict';

  // init LocalForage
  localforage.config({
    name: 'sehat',
    storeName: 'sehat_store',
  });

  // Initialize Firebase
  var config = {
    apiKey: 'AIzaSyBvw-16_9h0aJ5rpab2-hJ84N5a85jAbq4',
    authDomain: 'emergency-2ca6a.firebaseapp.com',
    databaseURL: 'https://emergency-2ca6a.firebaseio.com',
    projectId: 'emergency-2ca6a',
    storageBucket: 'emergency-2ca6a.appspot.com',
    messagingSenderId: '916281526247'
  };
  firebase.initializeApp(config);
  const messaging = firebase.messaging();

  var app = {
    isLoading: true,
    currentUser: null,
    alerts: [],
    drawerTitle: $('.mdl-layout__drawer .mdl-layout-title'),
    drawerNav: $('.mdl-layout__drawer nav'),    
    content: $('main .page-content'),
  };

  // Callback fired if Instance ID token is updated.
  messaging.onTokenRefresh(function () {
    messaging.getToken()
      .then(function (refreshedToken) {
        console.log('Token refreshed.');
        if (app && app.currentUser) {
          let oldToken = app.currentUser.messagingToken;
          app.currentUser.messagingToken = refreshedToken;
          localforage
            .setItem('currentUser')
            .then(function() {
              // update token in server
              $.post('/updateToken', data, function (res) {
                console.log('New token saved in database');
                console.log(res);
              })
                .fail(function (err) {
                  console.error(err);
                });
            })
            .catch(console.error);
        }
      })
      .catch(function (err) {
        console.error('Unable to retrieve refreshed token ', err);
      });
  });

  // accept message when on foreground
  messaging.onMessage(function (payload) {
    console.log('Message received.', payload);

    // save Alert in storage
    app.alerts = [];
    localforage
      .getItem('alerts')
      .then(function (result) {
        app.alerts = result ? result : [];
        app.alerts.push(payload.data);
        return localforage.setItem('alerts', app.alerts);
      })
      .then(function () {
        // display map
        routie('viewAlert/' + payload.data.id);
      })
      .catch(console.error);    

  });


  /**
   * Set currentUser in global app variable and localStorage
   * @param {*} currentUser 
   */
  app.setCurrentUser = function(currentUser) {
    if (!currentUser) {
      delete app.currentUser;
      return localforage.removeItem('currentUser');
    } else {
      app.currentUser = currentUser;
      return localforage.setItem('currentUser', currentUser);
    }
  }

  /**
   * Get localstorage currentUser
   */
  app.getCurrentUser = function() {    
    return localforage.getItem('currentUser');
  }

  /**
   * Enable firebase messaging notification
   */
  app.enableNotification = function() {
    // request permission
    return messaging
      .requestPermission()
      .then(function () {
        // get token
        return messaging.getToken();
      });
  };


  /**
   * Register as new user
   * @param {*} data 
   */
  app.registerUser = function(data) {
    return new Promise(function(resolve, reject) {
      $.post('/register', data, function (res) {
          resolve(res);
        })
        .fail(function (err) {
          reject(err.responseText || 'Error occured. Please contact administrator');
        });
    });
  };


  /**
   * Login as existing user
   * @param {*} data 
   */
  app.login = function (data) {
    return new Promise(function (resolve, reject) {
      $.post('/login', data, function (res) {
        resolve(res);
      })
        .fail(function (err) {
          reject(err.responseText || 'Error occured. Please contact administrator');
        });
    });
  };


  /**
   * Logout current user
   */
  app.logout = function () {
    console.log('Logging out');
    app.setCurrentUser(undefined).then(function() { console.log('currentUser removed') });

    // modify
    app.drawerTitle.text('');
    
    // remove
    app.drawerNav.find('.panic, .add-member, .view-alert, .logout').hide();

    // display
    app.drawerNav.find('.login').show();

    // go to login page
    routie('login');
  };


  /**
   * Add member
   * @param {*} data 
   */
  app.addMember = function (data) {
    return new Promise(function (resolve, reject) {
      $.post('/addMember', data, function (res) {
        resolve(res);
      })
        .fail(function (err) {
          reject(err.responseText || 'Unable to add member. Please try again or contact administrator');
        });
    });
  };


  /**
   * Send alert to all members
   */
  app.sendAlert = function () {
    let data = {
      userId: app.currentUser._id,
      messagingToken: app.currentUser.messagingToken,
      lat: app.currentUser.position.lat,
      lng: app.currentUser.position.lng,
    };
    return new Promise(function (resolve, reject) {
      $.post('/sendAlert', data, function (res) {
        resolve(res);
      })
        .fail(function (err) {
          reject(err.responseText || 'Unable to send alert. Please try again or contact administrator');
        });
    });
  };

  
  /**
   * Render user menu
   */
  app.renderUserMenu = function() {
    // modify
    app.drawerTitle.text(app.currentUser.email);

    // hide
    app.drawerNav.find('.login').hide();

    // show
    app.drawerNav.find('.add-member, .view-alert, .panic, .logout').show();
  }


  /**
   * Render login/register form
   */
  app.renderLoginForm = function() {

    let loginForm = `
<form id="login-form" class="center" action="#">
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="email" id="email">
    <label class="mdl-textfield__label" for="email">Email</label>
  </div>
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="password" id="password">
    <label class="mdl-textfield__label" for="password">Password</label>
  </div>
  <div>
  <button id="btn-login" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
    Login
  </button>  
  <button id="btn-register" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
    Register
  </button>
  </div>
</form>`;

    app.content.html(loginForm);        

    // register button click handler
    $('#btn-register', app.content).on('click', function () {
      let data = {
        email: $('#email').val(),
        password: $('#password').val(),
      };
      let self = $(this);
      let initialText = self.text();
      self.prop('disabled', true).text('Processing');
      // make sure notification is enabled
      app
        .enableNotification()
        .then(function(token) {
          console.log('Token:' + token);
          data.messagingToken = token;
          return app.registerUser(data);
        })
        .then(function(user) {
          console.log('Registration successful', user);          
          return app.setCurrentUser(user);          
        })
        .then(function() {
          // display home          
          routie('/');
        })
        .catch(function(err) {
          alert(err.responseText || err || 'Unable to activate notification');
          self.prop('disabled', false).text(initialText);          
        });

      return false;
    });    

    // login button click handler
    $('#btn-login', app.content).on('click', function () {
      let data = {
        email: $('#email').val(),
        password: $('#password').val(),
      };
      let self = $(this);
      let initialText = self.text();
      self.prop('disabled', true).text('Processing');
      // make sure notification is enabled
      app
        .enableNotification()
        .then(function(token) {
          console.log('Token:' + token);
          data.messagingToken = token;
          return app.login(data);
        })
        .then(function(user) {
          console.log('Login successful', user);
          return app.setCurrentUser(user);
        })
        .then(function() {
          routie('/');
        })
        .catch(function (err) {
          alert(err.responseText || err || 'Unable to activate notification');
          self.prop('disabled', false).text(initialText);
        });
      return false;
    });    

    // re-register Material Lite components
    componentHandler.upgradeAllRegistered();
  };

  /**
   * Render Panic button form
   */
  app.renderPanicButton = function () {
    let panicForm = `
    <audio id="audio-help" controls style="display:none" loop="true">
      <source src="/audio/help.mp3" type="audio/mpeg">
    </audio>
    <div id="panic-form" class="center">
      <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--colored">
        <strong>HELP</strong>
      </button>
    </div>
    <br><br><br>
    <div class="center">
      <button id="btn-guide" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
        Guide for Helper
      </button>
    </div>`;

    app.content.html(panicForm);

    // event handlers
    $('#btn-guide', app.content).on('click', function () {
      $('#audio-help').trigger('pause');
      // render guide for helper
      // app.renderHelp();
      routie('help');
      return false;
    });
    
    $('#panic-form button', app.content).on('click', function() {
      let self = $(this);

      // STOP
      if (self.text() == 'STOP') {
        self.addClass('mdl-button--colored').text('HELP');
        $('#audio-help').trigger('pause');
        return false;
      } else {
        // change color & label
        self.removeClass('mdl-button--colored').text('STOP');

        // play audio
        $('#audio-help').trigger('play');

        // get geolocation
        navigator.geolocation.getCurrentPosition(
          function(p) {
            console.log(p);
            app.currentUser.position = {
              lat: p.coords.latitude,
              lng: p.coords.longitude,
            };
            // send alert with location
            app.sendAlert().then(console.log).catch(function (err) { alert(err.responseText || err || 'Unable to send alert'); });
          }, 
          function(err) {
            console.error(err);
            // send alert without location
            let msg = 'Unable to find GPS location. Alert was sent without location first. Please try again later';
            app.sendAlert().then(function(res) { console.log(res); alert(msg); }).catch(function (err) { alert(err.responseText || err || 'Unable to send alert'); });  
          }, 
          { enableHighAccuracy: true });        

        return false;
      }
    });
  };


  /**
   * Render member list
   */
  app.renderMemberList = function(container) {
    container = container || $('.mdl-list', app.content);
    let memberList = '';
    app.currentUser.members.forEach(function (email) {
      memberList += `
<div class="mdl-list__item">
  <span class="mdl-list__item-primary-content">
    <i class="material-icons mdl-list__item-avatar">person</i>
    <span>${email}</span>
  </span>
  <a data-email="${email}" class="mdl-list__item-secondary-action remove-member" href="javascript:"><i class="material-icons">highlight_off</i></a>
</div>
`;
    });
    container.html(memberList);

    // event handler
    $('.remove-member').on('click', function() {
      let self = $(this);
      let memberEmail = self.data('email');
      if (confirm('Remove ' + memberEmail + '?')) {
        self.parent().remove();
        let data = {
          userId: app.currentUser._id,
          messagingToken: app.currentUser.messagingToken,
          memberEmail: memberEmail
        };
        // remove on server
        $.post('/removeMember', data, function(members) {
          console.log(members);
          app.currentUser.members = members;
          app
            .setCurrentUser(app.currentUser)
            .then(app.renderMemberList);
        })
        .fail(function(err) {
          let errMsg = err.responseText || err;
          alert('Failed to remove ' + memberEmail + ': ' + errMsg);
          console.error(err);
        });
      }
      return false;
    });
  };


  /**
 * Render add member form
 */
  app.renderAddMemberForm = function () {
    // render form
    let addMemberForm = `
<form action="#">
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="email" id="member-email">
    <label class="mdl-textfield__label" for="member-email">Email</label>
  </div>
  <button id="btn-add-member" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accents">
    Add
  </button>
</form>
<div class="mdl-list"></div>`;    
    app.content.html(addMemberForm);
    app.renderMemberList();

    // event handlers
    $('#btn-add-member', app.content).on('click', function() {
      let data = {
        memberEmail: $('#member-email').val(),
        userId: app.currentUser._id,
        messagingToken: app.currentUser.messagingToken,
      };
      let self = $(this);
      let initialText = self.text();
      self.prop('disabled', true).text('Processing');
      // make sure notification is enabled
      app
        .addMember(data)
        .then(function (members) {
          console.log(members);
          self.prop('disabled', false).text(initialText);
          $('#member-email').val('');
          // update member list
          app.currentUser.members = members;
          return app.setCurrentUser(app.currentUser);          
        })
        .then(function() {
          // render member list in UI
          app.renderMemberList($('.mdl-list', app.content));
        }) 
        .catch(function (err) {
          alert(err.responseText || err || 'Unable to activate notification');
          self.prop('disabled', false).text(initialText);
        });
      return false;      
    });

    componentHandler.upgradeAllRegistered();
  };


  /**
   * Render alert view
   */
  app.renderAlertView = function(id) {
    // make sure only run once
    // due to the bug on routie
    if (app.content.find('#map').length > 0) {
      console.log('alert view has been rendered');
      return false;
    }

    console.log('rendering alert view');

    // render alert
    // TODO: load & display closest hospitals from Google Places API
    let content = `
<div id="alert-view">
  <div class="form-control">
    <label>Alert location</label> (<span id="label-coordinate">Loading..</span>)
  </div>
  <div id="map"></div>
  <div class="form-control">
    <label>Sender</label>
    <div id="label-sender">Loading..</div>
  </div>
  <div class="form-control">
    <label>Time</label>
    <div id="label-time">Loading..</div>
  </div>
  <div class="form-control">
    <label>Message</label>
    <div id="label-message">Loading..</div>
  </div>
  <div id="list-hospitals" class="form-control">
    <label>Nearest hospitals</label>
    <div>Loading..</div>
  </div>
</div>
`;
    app.content.html(content);

    // load from storage
    localforage
      .getItem('alerts')
      .then(function(alerts) {
        // find alert id
        let currentAlert = null;
        if (!id) {
          if (!alerts || alerts.length <= 0) {
            return Promise.reject(new Error('No alert yet'));
          }
          // get the latest one
          currentAlert = alerts[alerts.length - 1];
        } else {
          for (let i = 0; i < alerts.length; i++) {
            if (alerts[i].id == id) {
              currentAlert = alerts[i];
              break;
            }
          }
        }

        if (!currentAlert) {
          return Promise.reject(new Error('Alert not found! ID ' + id));
        }
        // parse to coordinate to float
        currentAlert.lat = parseFloat(currentAlert.lat);
        currentAlert.lng = parseFloat(currentAlert.lng);

        // display data
        app.content.find('#label-sender').text(currentAlert.sender);
        app.content.find('#label-time').text(currentAlert.createdAt);
        app.content.find('#label-message').text(currentAlert.body);
        app.content.find('#label-coordinate').text(currentAlert.lat + ',' + currentAlert.lng);
        
        // get nearby hospitals
        let data = {
          userId: app.currentUser._id,
          messagingToken: app.currentUser.messagingToken,
          lat: currentAlert.lat,
          lng: currentAlert.lng,
        };
        $.getJSON('/nearbyHospitals', data, function(hospitals) {
          let listHtml = $('<ol>');
          hospitals.forEach(function(h) {
            listHtml.append($('<li>').text(`${h.name}, ${h.address}`));
          });
          // clear and append new content
          app.content.find('#list-hospitals div, #list-hospitals ol').remove();
          app.content.find('#list-hospitals').append(listHtml);
        })
        .fail(function (err) {
          console.error(err);
          let divHtml = $('<div>').text(err.responseText || 'Unable to get nearby hospitals');
          app.content.find('#list-hospitals div, #list-hospitals ol').remove();
          app.content.find('#list-hospitals').append(divHtml);
        });

        // render map
        new Promise(function (resolve, reject) {
          // assign global var
          resolveFunc = resolve;
        })
          .then(function () {
            let center = { lat: currentAlert.lat, lng: currentAlert.lng };
            let map = new google.maps.Map(app.content.find('#map')[0], {
              zoom: 17,
              center: center
            });
            let marker = new google.maps.Marker({
              position: center,
              map: map
            });
          });

        // create google map script that trigger global initGoogleMap()
        // TODO: get API key from server
        $('head').append($('<script>')
          .attr('type', 'text/javascript')
          .attr('src', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCn8FCOYO6GLTu61CT1BlY88cEOBCruRsg&callback=initGoogleMap'));
          
      })
      .catch(function(err) {
        alert(err.message || err);
        console.error(err);
        routie('/')
      });
  
  };


  /**
   * Render Help card
   */
  app.renderHelp = function () {
    let card = `
<div class="mdl-card">
  <div class="mdl-card__title">
    <h2 class="mdl-card__title-text">Panduan</h2>
  </div>
  <div id="guide" class="mdl-card__supporting-text">    
    <p>Bila kita menemui ada orang yang tidak sadarkan diri setelah jatuh/kecelakaan/tiba-tiba tidak sadarkan diri misal karena serangan jantung. Pada kondisi kita akan memberikan pertolongan pertama/ bantuan hidup dasar hingga pihak layanan kesehatan datang ke lokasi korban untuk memberikan bantuan hidup lanjutan.</p>
    <img src="/images/guide/diagram_petunjuk.png" width="100%">    
    <p>Apa hal yang mesti kita lakukan?</p>
    <ol>
    <li><strong>Amankan lokasi korban dan diri anda dari situasi yang mungkin membahayakan<br /></strong>Contohnya: Meminta orang sekitar lokasi untuk mengatur lalu lintas di sekitar korban dan anda, selama memberikan pertolongan.</li>
    <li><strong>Periksa kondisi korban</strong>
    <ul>
    <li>Coba untuk membangunkan korban dengan memanggil dengan suara lantang dan mengguncangkannya seperti membangunkan orang dari tidur.</li>
    <img src="/images/guide/bhd_cek_kesadaran.png" width="100%">
    <li>Bila tidak bangun, lakukan perangsangan rasa nyeri pada korban dengan cara menekan dada antara kedua puting susu dengan jari (jangan terlalu keras). Selain itu bisa juga dengan menekan bagian kuku ibu jari korban.</li>
    <li>Bila tidak ada respon, periksa pernafasan dan denyut nadi korban, serta pstikan tidak ada sumbatan benda bada mulut/jalan nafas korban. Caranya, buka pakaian atas korban. Tempelkan telinga pada mulut korban (untuk mendengarkan suara nafas) dengan mata menghadap ke arah dada korban untuk melihat pergerakan dinding dada. Tangan memegang arteri karotis (leher) untuk memeriksa denyut nadi korban.</li>
    <img src="/images/guide/bhd_perhatikan_pergerakan_dada.png" width="65%">
    <img src="/images/guide/bhd_cek_denyut_nadi.jpg" width="30%">
    <p class="caption">Gambar 1: Periksa denyut nadi</p>
    
    <li>Bila tidak ada tanda-tanda adanya nafas dan nadi korban, segera berikan Bantuan Hidup Dasar (BHD) dan memanggil bantuan layanan kesehatan.</li>
    </ul>
    </li>
    <li><strong> Panggil bantuan</strong><br />Tunjuk seseorang dan pastikan dia memanggil bantuan seprti ambulans. Contoh: &ldquo;Bapak yang pakai baju biru kotak-kotak, segera telepon ambulans!&rdquo;</li>
    <li><strong>Berikan Bantuan Hidup Dasar (BHD).</strong> Dalam hal ini Resusitasi Jantung Paru (RJP) sambil menunggu bantuan datang.
    <ul>
    <li>RJP dilakukan dengan cara melakukan kompresi/memompa jantung dari luar dengan setiap siklusnya berupa melakukan penekanan pada dada sebanyak 30 kali (kecepatan 2x/detik) dan memberikan nafas buatan dari mulut ke mulut sebanyak 2 kali. Lakukan ini sebanyak 5 siklus atau hingga 30 menit atau hingga penolong tidak sanggup.</li>
    <li>Kompresi dada, letakkan tangan seperti gambar dengan posisi tegak lurus antara dada, tangan dan bahu. Kemudian lakukan penekanan agak dalam sekitar 2/3 dengan menggunakan bahu, sehingga saat menekan siku tetap lurus. Lakukan sebanyak 30 kali dengan kecepatan 2x/detik (100x/menit).</li>

    <img src="/images/guide/bhd_kompresi_jantung_posisi_tangan.jpg" width="100%">
    <img src="/images/guide/bhd_kompresi_jantung_posisi_tubuh.jpg" width="100%">
    <p class="caption">Gambar 2: Posisi untuk kompresi jantung luar</p>

    <li>Ekstensi kan leher dan angkat dagu (dorong ke arah kepala), sehangga kondisi korban seperti menengadah. Hal ini agar jalur nafas pasien terbuka.</li>

    <img src="/images/guide/bhd_buka_jalan_napas.jpg" width="100%">
    <p class="caption">Gambar 3: Membuka jalan napas</p>

    <li>Buka mulut dengan menarik dagu kebawah, kemudian tarik dagu kebawah sehingga mulut terbuka. Segera berikan nafas buatan dengan meniupkan sekuat tenaga hingga dada mengembang. Lakukan sebanyak 2 kali.</li>
    <li>Nilai kembali pernafasan dan denyut nadi arteri karotis, untuk memastikan pasien sudah bernfas dan berdetak kembali jantungnya.</li>
    <li>Dari 30 kompresi dan 2 nafas buatan disebut satu siklus. Biasanya dilakukan hingga 5 siklus atau 30 menit atau hingga penolong sudah tidak mampu lagi/pasien sudah kembali bernafas spontan.</li>
    <li>Apabila korban telah bernafas spontan, posisikan korban dalam posisi mantap.</li>    

    <img src="/images/guide/bhd_langkah_posisi_mantap.png" width="100%"/>
    <p class="caption">Gambar 4: Langkah memposisikan posisi mantap/recovery</p>

    <img src="/images/guide/bhd_posisi_mantap.jpg" width="100%"/>
    <p class="caption">Gambar 5: Posisi mantap/recovery</p>

    </ul>
    </li>
    </ol>
    <p>Bantuan Hidup Dasar (BHD) dikenal juga dengan sebutan&nbsp;<em>Basic Life Support</em> (BLS)&nbsp;dan Resusitasi Jantung Paru (RJP)</p>
  </div>
</div>    
`;
    app.content.html(card);
  };

  /**
   * Render About card
   */
  app.renderAbout = function () {
    let card = `
<div class="mdl-card">
  <div class="mdl-card__title">
    <h2 class="mdl-card__title-text">About</h2>
  </div>
  <div id="about" class="mdl-card__supporting-text">
    <p>Dirancang oleh Mahasiswa Pascasarjana Fakultas Ilmu Keperawatan Universitas Indonesia:</p>
    <ol>
    <li>Andi Sudrajat , NPM. 1706096216, Peminatan Keperawatan KMB</li>
    <img src="/images/about/andi_sudrajat.jpg">
    <li>Casman, NPM. 1706096241, Peminatan Keperawatan Anak</li>
    <img src="/images/about/casman.jpg">
    <li>Ita Pursitasari, NPM 1706096374, Peminatan Keperawatan Anak</li>
    <img src="/images/about/ita_pursitasari.jpg">
    <li>Juliana Gracia Eka P. Massie , NPM. 1706006933, Peminatan Keperawatan KMB</li>
    <img src="/images/about/juliana_gracia.jpg">
    <li>Ni Luh Putu Dian Yunita Sari , NPM. 1706096443, Peminatan Keperawatan Komunitas</li>
    <img src="/images/about/ni_luh_putu.jpg">
    <li>Siti Aisah, NPM. 1706096563, Peminatan Keperawatan KMB</li>
    <img src="/images/about/siti_aisah.jpg">
    <li>Suyatno, NPM. 1706096582, Peminatan Keperawatan Jiwa</li>
    <img src="/images/about/suyatno.jpg">
    </ol>
  </div>
</div>    
`;
    app.content.html(card);
  };
  
  /**
   * Reroute to login if not yet authenticated
   * @param {*} cb 
   */
  app.routeAuth = function(cb) {
    if (!app.currentUser) {
      routie('login');
    } else {
      app.renderUserMenu();
      cb();
    }    
  }

  app.init = function() {

    // auto close sidebar
    $(app.drawerNav).on('click', function () {
      $('.mdl-layout__drawer').toggleClass('is-visible');
      $('.mdl-layout__obfuscator').toggleClass('is-visible');
    });

    // load service worker for caching
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker
        .register('./service-worker.js')
        .then(function() { console.log('Service Worker Registered'); });
    }

    /** Routing **/

    // load from storage
    app
      .getCurrentUser()
      .then(function (currentUser) {
        app.currentUser = currentUser;

        routie('login', function (data) {
          if (app.currentUser) {
            routie('/');
          } else {
            console.log('No currentUser');
            app.renderLoginForm();
          }
        });
        routie('viewMember', function () { app.routeAuth(app.renderAddMemberForm); });        
        routie('viewAlert/:id?', function() { app.routeAuth(app.renderAlertView); });
        routie('', function() { app.routeAuth(app.renderPanicButton); });
        routie('logout', app.logout);
        routie('help', app.renderHelp);
        routie('about', app.renderAbout);

      });
    
  }

  // start app!
  app.init();

})();
