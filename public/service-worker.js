const version = '1.0.0';
var cacheName = 'sehat-' + version;
var filesToCache = [
    '/',
    '/index.html',
    '/scripts/app.js',
    '/scripts/routie.min.js',
    '/scripts/localforage.min.js',
    '/stylesheets/app.css',
    '/stylesheets/material.min.css',
    '/images/icons/icon-256x256.png',
    '/images/icons/icon-192x192.png',
    '/images/icons/icon-152x152.png',
    '/images/icons/icon-144x144.png',
    '/images/icons/icon-128x128.png',
    '/images/icons/icon-32x32.png',
    '/audio/help.mp3',
    'https://fonts.googleapis.com/icon?family=Material+Icons',
    'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js',
];

self.addEventListener('install', function(e) {
    console.log('[ServiceWorker] Install');
    e.waitUntil(
        caches.open(cacheName).then(function(cache) {
            console.log('[ServiceWorker] Caching app shell');
            return cache.addAll(filesToCache);
        })
    );
});

self.addEventListener('activate', function(e) {
    console.log('[ServiceWorker] Activate');
    e.waitUntil(
        caches.keys().then(function(keyList) {
            return Promise.all(keyList.map(function(key) {
                if (key !== cacheName) {
                    console.log('[ServiceWorker] Removing old cache', key);
                    return caches.delete(key);
                }
            }));
        })
    );
    return self.clients.claim();
});

self.addEventListener('fetch', function(e) {
    console.log('[ServiceWorker] Fetch', e.request.url);
    e.respondWith(
        caches.match(e.request).then(function(response) {
            return response || fetch(e.request);
        })
    );
});